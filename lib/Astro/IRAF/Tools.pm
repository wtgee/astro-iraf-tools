package Astro::IRAF::Tools;
# ABSTRACT: a set of tools for working with IRAF.

=head1 SYNOPSIS

Astro::IRAF::Tools is a collection of tools for working with 

various aspects of IRAF. In some cases these tools are for working

with generated data, in other cases they are useful for generating

data. See individual tools for description.

=cut

use 5.010;
use autobox;
use Moose;

extends qw(MooseX::App::Cmd);

1;
