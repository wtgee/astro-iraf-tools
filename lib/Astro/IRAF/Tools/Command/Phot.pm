package Astro::IRAF::Tools::Command::Phot;

# ABSTRACT: Tools for manipulating a results file created by
# the digiphot apphot phot tool.

=head1 SYNOPSIS

Processes a single result file from the phot tool and puts

the results into a json structure.

=cut

use Moose;
use 5.010;
extends qw(MooseX::App::Cmd::Command);

with qw/
  Astro::IRAF::Tools::Roles::Log
  Astro::IRAF::Tools::Roles::Files
  Astro::IRAF::Tools::Roles::Graph
  /;

use Try::Tiny;
use JSON;

has 'results' => (
    is      => 'rw',
    isa     => 'ArrayRef[Any]',
    traits  => ['Array'],
    default => sub { [] },
    handles => { 'add_result' => 'push', 'get_result' => 'shift' }
);

=method execute

Process all the input files.

=cut

sub execute {
    my ( $self, $opt, $args ) = @_;

    $self->log_info("Starting main run for phot tools.");

    $self->read_files if ( $self->has_files );
    $self->read_dir   if ( $self->has_dir );

	$self->export_json('phot_results.json',$self->results);
    $self->graph;

    $self->log_info("Done");

}


=method _parse_line

Handles the individual phot lines.

=cut

sub _parse_line {
    my ( $self, $line ) = @_;

    # NOTE: This should be done with something like
    # Regexp::Grammars
    my $re = qr{
		(?<filename>.{23})
		(?<xinit>.{10})
		(?<yinit>.{10})
		(?<id>.{6})
		(?<coords>.{23})
		(?<lid>.{6})
		(?<xcenter>.{14})
		(?<ycenter>.{11})
		(?<xshift>.{8})
		(?<yshift>.{8})
		(?<xerr>.{8})
		(?<yerr>.{15})
		(?<cier>.{5})
		(?<cerror>.{9})
		(?<msky>.{18})
		(?<stdev>.{15})
		(?<sskew>.{15})
		(?<nsky>.{7})
		(?<nsrej>.{9})
		(?<sier>.{5})
		(?<serror>.{9})
		(?<itime>.{18})
		(?<airmass>.{15})
		(?<ifilter>.{23})
		(?<otime>.{23})
		(?<rapert>.{12})
		(?<sum>.{14})
		(?<area>.{11})
		(?<flux>.{14})
		(?<mag>.{9})
		(?<merr>.{6})
		(?<pier>.{5})
		(?<perror>.{9})
	}xs;

    if ( $line =~ $re ) {
        my $lid     = $+{lid};
        my $mag     = $+{mag};
        my $airmass = $+{airmass};
        my $filter  = $+{ifilter};
        my $otime   = $+{otime};

        $lid     =~ s/\s//g;
        $mag     =~ s/\s//g;
        $airmass =~ s/\s//g;
        $filter  =~ s/\s//g;
        $filter  =~ s/Bessell-//g;
        $otime   =~ s/\s//g;

        $mag = 0 if ( $mag eq 'INDE' || $mag eq 'INDEF' );
        return if $mag == 0;

#$self->log_info(sprintf("lid: %s\tmag: %s\tairmass: %s\tfilter: %s\totime: %s",$lid,$mag,$airmass,$filter,$otime));

        $lid = sprintf( "%02d", $lid );

        my $result = {
            star_id => $lid,
            filter  => $filter,
            airmass => $airmass,
            mag     => $mag,
        };
        try {
            $self->add_result($result);
        }
        catch {
            $self->log_debug("Problem adding to results: $_");
        };
    }
}

1;
