package Astro::IRAF::Tools::Roles::Files;

use Moose::Role;
use IO::All -utf8;
use Try::Tiny;

requires '_parse_line';

has 'files' => (
    is            => 'rw',
    isa           => 'ArrayRef[Str]',
    traits        => [qw/Getopt/],
    predicate     => 'has_files',
    cmd_aliases   => 'f',
    documentation => 'Phot result files to be processed.'
);

has 'dir' => (
    is            => 'rw',
    isa           => 'Str',
    traits        => [qw/Getopt/],
    predicate     => 'has_dir',
    cmd_aliases   => 'd',
    documentation => 'Directory containing phot result files to be processed.'
);

=method read_dir

Read the contents of a dir.

=cut

sub read_dir {
    my ($self) = @_;

    try {
        my $dir = io( $self->dir );
        for my $file ( $dir->all ) {
            $self->log_info( "read_dir file: " . $file );
            $self->_parse_file($file);
        }
    }
    catch {
        $self->log_error("Problem reading the dir: $_");
    };
}

=method read_files

Read the files

=cut

sub read_files {
    my ($self) = @_;

    try {
        for my $file ( @{ $self->files } ) {
            $self->log_info( "read_file file: " . $file );
            $self->_parse_file($file);
        }
    }
    catch {
        $self->log_warn("Problem reading the dir: $_");
    };
}

=method _parse_file

=cut 

sub _parse_file {
    my ( $self, $filename ) = @_;

    my @lines;

    my $fh = IO::File->new;
    if ( $fh->open("< $filename") ) {

        my @tmp_lines;

        while ( my $line = <$fh> ) {
            chomp $line;
            next if $line =~ m/\A#/;

            push @tmp_lines, $line;

            if ( $line !~ m/\\/ ) {
                my $full_line = join '', @tmp_lines;
                $full_line =~ s/\\//g;

                push @lines, $full_line;

                @tmp_lines = ();
            }
        }
    }

    $self->_parse_line($_) for @lines;
}

=method export_json

Dumps an object to json.

=cut

sub export_json {
    my ( $self, $file, $obj ) = @_;

    return unless $file and $obj;

	$self->log_info("Exporting obj to json");
    my $json = JSON::encode_json($obj);

    try {
        $json > io($file);
    }
    catch {
        $self->log_debug(
            sprintf(
                "Problem exporting obj to json: %s\nfile: %s\nobj: %s",
                $_, $file, $obj
            )
        );

    };
}

no Moose::Role;
1;
