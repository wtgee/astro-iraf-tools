package Astro::IRAF::Tools::Roles::Log;

use Moose::Role;
use Try::Tiny;
use Log::Log4perl qw(:easy);
use Data::Printer;

with qw/MooseX::Log::Log4perl::Easy/;

BEGIN {
	Log::Log4perl->easy_init();
}

=head2 log_dump

Use Data::Printer to show an object.

=cut 

sub log_dump {
    my ($self, $item) = @_;

    $self->log_info(p $item);

}

no Moose::Role;
1;
