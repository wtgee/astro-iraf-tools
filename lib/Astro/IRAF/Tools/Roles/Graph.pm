package Astro::IRAF::Tools::Roles::Graph;

use Moose::Role;
use IO::All -utf8;
use List::Util;
use Try::Tiny;
use Graphics::Color::RGB;
use Chart::Clicker;
use Chart::Clicker::Data::Series;
use Chart::Clicker::Data::Range;
use Chart::Clicker::Data::DataSet;
use Chart::Clicker::Renderer::Point;
use Data::Printer;

with qw/
  Astro::IRAF::Tools::Roles::Log
  /;

=method graph

Outputs a graph of airmass vs mag.

=cut

sub graph {
    my ($self) = @_;

    my $all_chart = Chart::Clicker->new;

    # Set the domain and range
    my $domain = Chart::Clicker::Data::Range->new(
        {
            lower => 1.35,
            upper => 2.45
        }
    );

    my $red = Graphics::Color::RGB->new(
        {
            red   => .75,
            green => 0,
            blue  => 0,
            alpha => .8
        }
    );
    my $blue = Graphics::Color::RGB->new(
        {
            red   => 0,
            green => 0,
            blue  => .75,
            alpha => .8
        }
    );
    my $green = Graphics::Color::RGB->new(
        {
            red   => 0,
            green => .75,
            blue  => 0,
            alpha => .8
        }
    );

    for my $star ( keys %{ $self->results } ) {
        my $chart = Chart::Clicker->new;
        my @series;
        my ( $range_lower, $range_upper ) = ( 0, 0 );

        my $dataset = Chart::Clicker::Data::DataSet->new();

        # Build the color allocator
        my $color_allocator = Chart::Clicker::Drawing::ColorAllocator->new;

        my $color_lookup = {
            'Bessell-B' => $blue,
            'Bessell-R' => $red,
            'Bessell-V' => $green,
        };

        for my $filter ( keys %{ $self->results->{$star} } ) {
            next if $filter eq 'Bessell-B';
            my @airmass = sort keys %{ $self->results->{$star}->{$filter} };
            my @mags    = sort values %{ $self->results->{$star}->{$filter} };

            my $series = Chart::Clicker::Data::Series->new(
                name   => $filter,
                keys   => \@airmass,
                values => \@mags,
            );
            push @series, $series;
            $color_allocator->add_to_colors( $color_lookup->{$filter} );
            $dataset->add_to_series($series);

            my $tmp_upper = List::Util::max @mags;
            my $tmp_lower = List::Util::min @mags;

            $range_upper = $tmp_upper
              if ( $tmp_upper > $range_upper || $range_upper == 0 );
            $range_lower = $tmp_lower
              if ( $tmp_lower < $range_lower || $range_lower == 0 );

        }
        $range_lower = $range_lower - 0.2;
        $range_upper = $range_upper + 0.2;

        $self->log_info(
            sprintf( "lower: %f upper: %f", $range_lower, $range_upper ) );

        # assign the color allocator to the chart
        $chart->color_allocator($color_allocator);
		$all_chart->color_allocator($color_allocator);

        $chart->add_to_datasets($dataset);
        $all_chart->add_to_datasets($dataset);

        my $graph_name  = sprintf( "graphs/%s_mag_vs_airmass.png", $star );
        my $graph_title = sprintf( "Star %s Airmass vs Mag",       $star );
        $chart->title->text($graph_title);

        my $range = Chart::Clicker::Data::Range->new(
            {
                lower => $range_lower,
                upper => $range_upper,
            }
        );

        my $default_ctx = $chart->get_context('default');
        $default_ctx->domain_axis->range($domain);
        $default_ctx->range_axis->range($range);
        $default_ctx->renderer( Chart::Clicker::Renderer::Line->new );
        $default_ctx->renderer->shape(
            Geometry::Primitive::Circle->new( { radius => 3, } ) );
        $default_ctx->renderer->brush->width(1);

        $self->log_info("Generating graph for $graph_name");
        $chart->write_output($graph_name);
    }

    my $all_graph_name = sprintf("graphs/complete_mag_vs_airmass.png");
    $all_chart->title->text("Airmass vs Mag - All Stars");

    my $all_range = Chart::Clicker::Data::Range->new(
        {
            lower => 14,
            upper => 23,
        }
    );

    my $all_ctx = $all_chart->get_context('default');
    $all_ctx->domain_axis->range($domain);
    $all_ctx->range_axis->range($all_range);
    $all_ctx->renderer( Chart::Clicker::Renderer::Line->new );
    $all_ctx->renderer->shape(
        Geometry::Primitive::Circle->new( { radius => 3, } ) );
    $all_ctx->renderer->brush->width(1);

    $all_chart->legend->visible(0);

    $all_chart->write_output($all_graph_name);
}

no Moose::Role;
1;
