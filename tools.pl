#!/usr/bin/env perl

use 5.010;

use FindBin;
use lib "$FindBin::Bin/lib";

use Astro::IRAF::Tools;

my $tools = Astro::IRAF::Tools->new;
$tools->run;
